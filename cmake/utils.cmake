# Author Vinícius Gabriel Linden
# Date 2023-12-21

function(additional TARGET)
    add_custom_command(TARGET ${TARGET} POST_BUILD
        COMMAND ${CMAKE_SIZE} -G -d ${TARGET}.elf
        COMMENT "Displaying size for ${TARGET}"
        )

    add_custom_command(TARGET ${TARGET} POST_BUILD
        COMMAND ${CMAKE_OBJDUMP} -h -S "$<TARGET_FILE:${TARGET}>" >
        ${TARGET}.list
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Dumping compiled symbols of ${TARGET} to list"
        )

    add_custom_command(TARGET ${TARGET} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/compile_commands.json
        ${CMAKE_CURRENT_LIST_DIR}
        COMMENT "Copying compile_commands.json of ${TARGET} to root"
        )
endfunction()
