
# Template for Raspberry Pi Pico projects in C(++)

This is a really boilerplate for the Raspberry Pi Pico projects written in
C/C++. It includes:

- automatic pico-sdk download, when no path is given
- presets for CMake
- a convenience `build.sh` script
- Visual Studio Code settings

## Visual Studio Code

Recommended extensions:

- C/C++: C/C++ IntelliSense, debugging, and code browsing
- CMake Tools: Extended CMake support in Visual Studio Code
- Cortex-Debug: ARM Cortex-M GDB Debugger support for VSCode

```sh
code --install-extension ms-vscode.cpptools
code --install-extension ms-vscode.cmake-tools
code --install-extension marus25.cortex-debug
```