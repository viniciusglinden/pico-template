#!/usr/bin/env bash
# Author Vinícius Gabriel Linden
# Date 2023-12-18

set -eu
set -o pipefail

declare -r PROJECT="template"
declare -A DIR
DIR[project]="$(dirname -- "$(readlink -f -- "${BASH_SOURCE[0]}")")"
DIR[build]="${DIR[project]}/out"
DIR[sdk]="${DIR[project]}/pico_sdk"
DIR[data]="${DIR[project]}/data"
declare -r DIR
declare -A FILE
FILE[base]="${DIR[build]}/${PROJECT}"
FILE[uf2]="${FILE[base]}.uf2"
FILE[elf]="${FILE[base]}.elf"
FILE[hex]="${FILE[base]}.hex"
FILE[openocd]="${DIR[data]}/picoprobe.cfg"
declare -r FILE

_log() {
    echo "[${PROJECT}]: ${*}"
}

_clean() {
    rm -rf "${DIR[build]}" "${@:-}"
}

clean() {
    _log "cleaning cache"
    _clean
}

c() {
    clean
}

purge() {
    _log "purging all cache"
    _clean "${DIR[sdk]}"
}

_cmake_conf() {
    mkdir -p "${DIR[build]}"
    cmake "${*:-}" -S "${DIR[project]}" -B "${DIR[build]}" -G Ninja
}

release() {
    _log "building for release"
    _cmake_conf "-D CMAKE_BUILD_TYPE=Release"
    cmake --build "${DIR[build]}"
}

debug() {
    _log "building for debug"
    _cmake_conf "-D CMAKE_BUILD_TYPE=Debug"
    cmake --build "${DIR[build]}"
}

flash() {
    _log "programming RP2040 via picoprobe"
    openocd -s "${DIR[project]}/data" \
        -f picoprobe.cfg \
        -c "program ${FILE[elf]} verify reset exit"
}

gdb() {
    _log "starting gdb"
    gdb="gdb-multiarch"
    ! command -v "${gdb}" &>/dev/null &&
        gdb="arm-none-eabi-gdb"

    "${gdb}" \
        -iex "tar ext | openocd -f '${FILE[openocd]}' -c 'gdb_port pipe'" \
        -ex "load" \
        "${FILE[elf]}"
}

_main() {
    [[ "${#}" -eq 0 ]] &&
        _log "nothing to do" &&
        exit 0
    while [[ "${#}" -ne 0 ]]; do
        arg="${1}"
        ! type "${arg}" &>/dev/null &&
            _log "skipping argument ${arg}" &&
            shift &&
            continue
        "${arg}"
        shift
    done
}

_main "${@}"
